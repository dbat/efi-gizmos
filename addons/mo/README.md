Mo
==
"First there was Giz, and now theres Mo"

Efi,[br]
I hope this will help you get a start. Look in addons/mo and start in plugin.gd

I remarked here and there.

PS - Includes the addon called Quick Plugin Manager which gives you a menu on the top-right to quickly switch plugins on and off. Super helpful. Don't export it to wherever this is all going though.

PPS - Another tip. When you get super weird errors (like can't find script, or class name) then get into the habit of reloading - go to Project -> Reload Current Project. Super helpful to get rid of those kind of cache bugs.

PPPS - When all else fails and you are lost in a soup of bugs and can't make sense of them, from the project root: rm -fr .godot <-- that nukes the entire cache and a restart of the project has a better chance of success.

:D

HTH,[br]
dbat
