extends EditorNode3DGizmoPlugin

#Original Work Copyright (c) 2020-present HungryProton.
#Modified Work Copyright (c) 2023-present Donn Ingle.
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of this software
#and associated documentation files (the "Software"), to deal in the Software without
#restriction, including without limitation the rights to use, copy, modify, merge, publish,
#distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
#Software is furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all copies or
#substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
#BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
#NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
#DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## Much of this taken from code writte by HungryProton https://github.com/HungryProton/scatter


## Efi README
## ==
## This is the Editor Gizmo Plugin code
## It is initialized in plugin.gd
##
## It draws the gizmo and the 'handles' (I think they are the red circles.)
##
## TODO: Have noticed that I can't drag the handles too fast, they kind of
## lose track of the mouse. Surely some bug of mine.



# ref to our custom node 3d
const MyCustomNode3D := preload("mycustomnode.gd")


# for later stuff and things
var _undo_redo: EditorUndoRedoManager
var _plugin: EditorPlugin


## This seems to be where you associate your gizmo with
## your custom Node3D...
func _has_gizmo(node):
	return node is MyCustomNode3D


func _get_gizmo_name() -> String:
	return "dbatCustomGizmo"


## material stuff also pinched from HungryProton(HP)
func _init():
	create_material("main", Color(1, 0, 0))
	create_handle_material("handles")

	create_material("primary", Color(1, 0.4, 0))
	create_material("secondary", Color(0.4, 0.7, 1.0))
	create_material("tertiary", Color(Color.STEEL_BLUE, 0.2))
	create_custom_material("primary_top", Color(1, 0.4, 0))
	create_custom_material("secondary_top", Color(0.4, 0.7, 1.0))
	create_custom_material("tertiary_top", Color(Color.STEEL_BLUE, 0.1))

	create_material("inclusive", Color(0.9, 0.7, 0.2, 0.15))
	create_material("exclusive", Color(0.9, 0.1, 0.2, 0.15))

	var handle_icon = preload("./src/icons/main_handle.svg")
	var secondary_handle_icon = preload("./src/icons/secondary_handle.svg")
	create_handle_material("default_handle")
	create_handle_material("primary_handle", false, handle_icon)
	create_handle_material("secondary_handle", false, secondary_handle_icon)


# Creates a standard material displayed on top of everything.
# Only exists because 'create_material() on_top' parameter doesn't seem to work.
func create_custom_material(name: String, color := Color.WHITE):
	var material := StandardMaterial3D.new()
	material.set_blend_mode(StandardMaterial3D.BLEND_MODE_ADD)
	material.set_shading_mode(StandardMaterial3D.SHADING_MODE_UNSHADED)
	material.set_flag(StandardMaterial3D.FLAG_DISABLE_DEPTH_TEST, true)
	material.set_albedo(color)
	material.render_priority = 100

	add_material(name, material)


## Code pinched from HP
## It seems to draw the orange box we see.
func _redraw(gizmo: EditorNode3DGizmo):
	gizmo.clear()
	var gizmos_node3d = gizmo.get_node_3d()
	var shapesize = gizmos_node3d.shapesize

	### Draw the Box lines
	var lines = PackedVector3Array()
	var lines_material := get_material("primary_top", gizmo)
	var half_size = shapesize * 0.5

	var corners := [
		[ # Bottom square
			Vector3(-1, -1, -1),
			Vector3(-1, -1, 1),
			Vector3(1, -1, 1),
			Vector3(1, -1, -1),
			Vector3(-1, -1, -1),
		],
		[ # Top square
			Vector3(-1, 1, -1),
			Vector3(-1, 1, 1),
			Vector3(1, 1, 1),
			Vector3(1, 1, -1),
			Vector3(-1, 1, -1),
		],
		[ # Vertical lines
			Vector3(-1, -1, -1),
			Vector3(-1, 1, -1),
		],
		[
			Vector3(-1, -1, 1),
			Vector3(-1, 1, 1),
		],
		[
			Vector3(1, -1, 1),
			Vector3(1, 1, 1),
		],
		[
			Vector3(1, -1, -1),
			Vector3(1, 1, -1),
		]
	]

	var block_count = corners.size()
	#if not is_selected(gizmo):
	#	block_count = 1

	for i in block_count:
		var block = corners[i]
		for j in block.size() - 1:
			lines.push_back(block[j] * half_size)
			lines.push_back(block[j + 1] * half_size)

	gizmo.add_lines(lines, lines_material)
	gizmo.add_collision_segments(lines)

	### Fills the box inside
	var mesh = BoxMesh.new()
	mesh.size = shapesize

	var mesh_material: StandardMaterial3D
#	if gizmos_node3d.negative:
#		mesh_material = get_material("exclusive", gizmo)
#	else:
	mesh_material = get_material("inclusive", gizmo)

	gizmo.add_mesh(mesh, mesh_material)

	### Draw the handles, one for each axis
	var handles := PackedVector3Array()
	var handles_ids := PackedInt32Array()
	var handles_material := get_material("default_handle", gizmo)

	handles.push_back(Vector3.RIGHT * shapesize.x * 0.5)
	handles.push_back(Vector3.UP * shapesize.y * 0.5)
	handles.push_back(Vector3.BACK * shapesize.z * 0.5)

	gizmo.add_handles(handles, handles_material, handles_ids)


## No clue yet...
func set_undo_redo(ur: EditorUndoRedoManager) -> void:
	_undo_redo = ur


func set_editor_plugin(plugin: EditorPlugin) -> void:
	_plugin = plugin

## The "handlers" seem to be the little red circle grabby things.
func _get_handle_name(_gizmo: EditorNode3DGizmo, _handle_id: int, _secondary: bool) -> String:
	return "I_am_handler"


func _get_handle_value(_gizmo: EditorNode3DGizmo, _handle_id: int, _secondary: bool) -> Variant:
	return _gizmo.get_node_3d().shapesize


## Also pinched from HP.
func _set_handle(_gizmo: EditorNode3DGizmo, _handle_id: int, _secondary: bool, _camera: Camera3D, _screen_pos: Vector2) -> void:
	if _handle_id < 0 or _handle_id > 2:
		return

	var axis := Vector3.ZERO
	axis[_handle_id] = 1.0 # handle 0:x, 1:y, 2:z

	var shape_node = _gizmo.get_node_3d()
	var gt := shape_node.get_global_transform()
	var gt_inverse := gt.affine_inverse()

	var origin := gt.origin
	var drag_axis := (axis * 4096) * gt_inverse
	var ray_from = _camera.project_ray_origin(_screen_pos)
	var ray_to = ray_from + _camera.project_ray_normal(_screen_pos) * 4096

	var points = Geometry3D.get_closest_points_between_segments(origin, drag_axis, ray_from, ray_to)

	var size = shape_node.shapesize
	size -= axis * size
	var dist = origin.distance_to(points[0]) * 2.0
	size += axis * dist
	shape_node.shapesize = size


## Don't know yet..
func _commit_handle(_gizmo: EditorNode3DGizmo, _handle_id: int, _secondary: bool, _restore: Variant, _cancel: bool) -> void:
#	var shape = _gizmo.get_node_3d().shape
#	if _cancel:
#		shapesize = _restore
#		return
	pass

