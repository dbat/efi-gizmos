@tool
extends EditorPlugin

#Original Work Copyright (c) 2020-present HungryProton.
#Modified Work Copyright (c) 2023-present Donn Ingle.
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of this software
#and associated documentation files (the "Software"), to deal in the Software without
#restriction, including without limitation the rights to use, copy, modify, merge, publish,
#distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
#Software is furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all copies or
#substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
#BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
#NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
#DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## Much of this taken from code writte by HungryProton https://github.com/HungryProton/scatter


## Efi README
## ==
## This is the main entry point for the entire addon


# Our custom Node3D
const mycustomnode := preload("mycustomnode.gd")

# Our custom gizmo that will work for/with the custom Node 3D
const MyCustomGizmoPlugin = preload("my_custom_gizmo_plugin.gd")
# New it here.
var gizmo_plugin = MyCustomGizmoPlugin.new()


func _get_plugin_name():
	return "Mo" #first there was Giz, but he failed. Now there is Mo :D


func _enter_tree():
	# Here's our custom Node3D
	add_custom_type(
		"mycustomnode",
		"Node3D",
		mycustomnode,
		preload("./icons/icon.svg")
	)
	# Here's our custom Gizmo
	add_node_3d_gizmo_plugin(gizmo_plugin)


# Murderize all the things
func _exit_tree():
	remove_custom_type("mycustomnode")
	remove_node_3d_gizmo_plugin(gizmo_plugin)


func _handles(node) -> bool:
	return node is mycustomnode
